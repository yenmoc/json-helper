# Json Helper

## Installation

```bash
"com.yenmoc.json-helper":"https://gitlab.com/yenmoc/json-helper"
or
npm publish --registry=http://localhost:4873
```

## Usages

```csharp
JsonHelper.FromJson<T>(JsonHelper.FixJson("data"));
```

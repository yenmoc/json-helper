# Changelog

## [0.0.2] - 10-08-2019
### Added
	-T[] FromJson<T>(string json)
	-string ToJson<T>(T[] array)
	-string FixJson(string value)
	-string RemoveItems(string value)
	-string RemoveArray(string value)

